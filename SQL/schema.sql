CREATE TABLE TOPIC(T_ID INT(255) PRIMARY KEY AUTO_INCREMENT,
   TNAME VARCHAR(200)
);

CREATE TABLE QUESTION(Q_ID INT(255) PRIMARY KEY AUTO_INCREMENT,
   QUESTION TEXT,
   TOPIC_ID INT(255),
   DIFFICULTY TINYINT(5), 
   FOREIGN KEY(TOPIC_ID) REFERENCES TOPIC(T_ID)
);

CREATE TABLE ANSWER(A_ID INT(255) PRIMARY KEY AUTO_INCREMENT,
   Q_ID INT(255),
   ANSWER TEXT,
   FOREIGN KEY(Q_ID) REFERENCES QUESTION(Q_ID)
);

CREATE TABLE MCQ(Q_ID INT(255) PRIMARY KEY AUTO_INCREMENT,
   QUESTION TEXT,
   TOPIC_ID INT(255),
   DIFFICULTY TINYINT(5), 
   FOREIGN KEY(TOPIC_ID) REFERENCES TOPIC(T_ID)
);

CREATE TABLE MCA(A_ID INT(255) PRIMARY KEY AUTO_INCREMENT,
   Q_ID INT(255),
   CHOICES TEXT,
   ISCORRECTANS BOOLEAN,
   FOREIGN KEY(Q_ID) REFERENCES MCQ(Q_ID)
);

INSERT INTO TOPIC(TNAME) VALUES('Java');
INSERT INTO TOPIC(TNAME) VALUES('UML');