package fr.epita.quizmgr.Launcher;

import java.util.Scanner;
import fr.epita.quizmgr.services.dao.QuizMgrDAO;
import fr.epita.quizmgr.utility.QuizMgrUtility;

/**
 * @author KadySwar
 *
 */
public class Launcher {

	private static final String UNAME = "KS";
	private static final String PWD = "KSPWD";

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(" ********** Welcome to my application !  ********** ");
		System.out.println(" ********** Kindly enter your details !  ********** ");
		String username = getResp(scanner, "User Name :");
		if (UNAME.equals(username)) {
			adminLogin(scanner);
		} else {
			studentLogin(scanner);
		}
		scanner.close();
	}

	private static void adminLogin(Scanner scanner) {
		QuizMgrUtility qmUtil = new QuizMgrUtility();
		String password = getResp(scanner, "Password : ");
		if (authenticateAdmin(password)) {
			boolean exit = false;
			do {
				System.out.println(" ********** Welcome Admin ********** ");
				System.out.println("Please choose an action\n1.Create \n2.Search \n3.Update \n4.Delete \n5.Exit \n");
				String option = getResp(scanner, "Please input ur choice 1|2|3|4|5");
				switch (option) {
				case "1":
					qmUtil.createQuestionAnswers(scanner);
					break;
				case "2":
					qmUtil.searchQuestionAns(scanner);
					break;
				case "3":
//					qmUtil.updateQuestion(scanner, daoObj);
					break;
				case "4":
					// call delete method
					break;
				case "5":
					option = getResp(scanner, "Do you confirm to exit ?! ");
					exit = option.equalsIgnoreCase("Y");
					System.out.println(" ********** Exiting application ! ********** ");
				default:
					break;
				}
			} while (!exit);
		} else {
			System.out.println("!!! Invalid credentials, exiting application  ********** ");
		}
	}

	private static void studentLogin(Scanner scanner) {
		// TODO Auto-generated method stub

	}

	private static boolean authenticateAdmin(String password) {
		if (PWD.equals(password)) {
			return true;
		}
		return false;
	}

	private static String getResp(Scanner scanner, String proposedQuestion) {
		System.out.println("Kindly enter the " + proposedQuestion);
		return scanner.nextLine();
	}
}