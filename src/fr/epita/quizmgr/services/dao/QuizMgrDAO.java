package fr.epita.quizmgr.services.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.epita.quizmgr.datamodel.Answer;
import fr.epita.quizmgr.datamodel.MCA;
import fr.epita.quizmgr.datamodel.MCQ;
import fr.epita.quizmgr.datamodel.Question;
import fr.epita.quizmgr.datamodel.Topic;
import fr.epita.quizmgr.services.DBConfiguration;

public class QuizMgrDAO {

	private static final String INSERT_QUERY_Q = "insert into tableName (QUESTION,DIFFICULTY,TOPIC_ID) VALUES (?, ?, ?)";
	private static final String UPDATE_QUERY_R = "update tableName set QUESTION=?,DIFFICULTY=?,TOPIC_ID=? WHERE Q_ID=?";
	private static final String DELETE_QUERY_R = "delete tableName WHERE Q_ID=?";
	private static final String SEARCH_BY_QUESTION_QUERY = "select Q_ID,QUESTION,DIFFICULTY,T_ID,TNAME from tableName q,TOPIC tp where q.TOPIC_ID=tp.T_ID and QUESTION LIKE ?";
	private static final String SEARCH_BY_TOPIC_QUERY = "select Q_ID,QUESTION,DIFFICULTY,T_ID,TNAME from tableName q,TOPIC tp where q.TOPIC_ID=tp.T_ID and tp.TNAME like ?";
	private static final String SEARCH_ANSWER = "select A_ID,ANSWER from ANSWER where Q_ID= ?";
	private static final String MCA = "select A_ID,CHOICES from MCA where Q_ID= ?";

	private Connection getConnection() throws SQLException {

		DBConfiguration config = DBConfiguration.getInstance();

		String url = config.getConfigValue("jdbc.url");
		String username = config.getConfigValue("jdbc.username");
		String password = config.getConfigValue("jdbc.password");

		return DriverManager.getConnection(url, username, password);
	}

	public List<Question> searchByTopic(Question question) {

		List<Question> questions = new ArrayList<>();
		try {
			Connection connection = getConnection();
			String topicName = "Java";
			if (question.getTopicID() == 2) {
				topicName = "UML";
			}

			PreparedStatement readStmt;
			if (question instanceof MCQ)
				readStmt = connection.prepareStatement(SEARCH_BY_TOPIC_QUERY.replace("tableName", "MCQ"));
			else
				readStmt = connection.prepareStatement(SEARCH_BY_TOPIC_QUERY.replace("tableName", "Question"));
			readStmt.setString(1, topicName);

			ResultSet rs = readStmt.executeQuery();
			while (rs.next()) {
				int qId = rs.getInt(1);
				String questionLabel = rs.getString(2);
				int difficulty = rs.getInt(3);
				int topicID = rs.getInt(4);
				Question currentQuestion = new Question();
				currentQuestion.setQuestion(questionLabel);
				currentQuestion.setDifficulty(difficulty);
				currentQuestion.setTopicID(topicID);
				Topic tpic = new Topic();
				String tname = rs.getString(5);
				tpic.setTopicName(tname);
				currentQuestion.setTopic(tpic);
				questions.add(currentQuestion);
			}
			readStmt.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questions;
	}

	public List<Question> searchByQuestion(Question question) {
		List<Question> questions = new ArrayList<>();
		try {
			PreparedStatement readStmt;
			Connection connection = getConnection();
			if (question instanceof MCQ)
				readStmt = connection.prepareStatement(SEARCH_BY_QUESTION_QUERY.replace("tableName", "MCQ"));
			else
				readStmt = connection.prepareStatement(SEARCH_BY_QUESTION_QUERY.replace("tableName", "Question"));

			readStmt.setString(1, "%" + question.getQuestion() + "%");

			ResultSet rs = readStmt.executeQuery();
			while (rs.next()) {
				int qid = rs.getInt(1);
				String questionLabel = rs.getString(2);
				int difficulty = rs.getInt(3);
				int topicID = rs.getInt(4);
				String tname = rs.getString(5);
				Question currentQuestion = new Question();
				currentQuestion.setQuestionID(qid);
				currentQuestion.setQuestion(questionLabel);
				currentQuestion.setDifficulty(difficulty);
				currentQuestion.setTopicID(topicID);
				Topic tpic = new Topic();
				tpic.setTopicName(tname);
				currentQuestion.setTopic(tpic);
				questions.add(currentQuestion);
			}
			readStmt.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questions;
	}

	public int create(Question question, MCQ mcq) {
		int updateCount = 0;
		try {
			Connection connection = getConnection();
			if (mcq != null) {
				PreparedStatement createStmt = connection.prepareStatement(INSERT_QUERY_Q.replace("tableName", "MCQ"));
				createStmt.setString(1, mcq.getQuestion());
				createStmt.setInt(2, mcq.getDifficulty());
				createStmt.setInt(3, mcq.getTopicID());
				updateCount = createStmt.executeUpdate();
				createStmt.close();
			} else {
				PreparedStatement createStmt = connection
						.prepareStatement(INSERT_QUERY_Q.replace("tableName", "Question"));
				createStmt.setString(1, question.getQuestion());
				createStmt.setInt(2, question.getDifficulty());
				createStmt.setInt(3, question.getTopicID());
				updateCount = createStmt.executeUpdate();
				createStmt.close();
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return updateCount;
	}

	public void createAnswerForOpenQuestion(Answer answer) {
		try {
			Connection connection = getConnection();
			PreparedStatement createAnswerStmt = connection
					.prepareStatement("insert into ANSWER (Q_ID,ANSWER) values (?,?)");
			createAnswerStmt.setString(2, answer.getAnswer());
			createAnswerStmt.setInt(1, answer.getQuestionID());
			createAnswerStmt.executeUpdate();
			createAnswerStmt.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createAnswerForMCQ(List<MCA> multipleAnswersList) {
		try {

			Connection connection = getConnection();
			PreparedStatement createAnswerStmt = connection
					.prepareStatement("insert into MCA (Q_ID,CHOICES,ISCORRECTANS) values (?,?,?)");
			for (MCA answer : multipleAnswersList) {
				createAnswerStmt.setString(2, answer.getAnswer());
				createAnswerStmt.setInt(1, answer.getQuestionID());
				createAnswerStmt.setBoolean(3, answer.isCorrectOption());				
				createAnswerStmt.executeUpdate();
			}
			createAnswerStmt.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void update(Question question) {
		try {
			Connection connection = getConnection();
			PreparedStatement updateStmt;
			if (question instanceof MCQ)
				updateStmt = connection.prepareStatement(UPDATE_QUERY_R.replace("tableName", "MCQ"));
			else
				updateStmt = connection.prepareStatement(UPDATE_QUERY_R.replace("tableName", "MCQ"));
			updateStmt.setString(1, question.getQuestion());
			updateStmt.setInt(2, question.getDifficulty());
			updateStmt.setInt(3, question.getTopicID());
			updateStmt.execute();
			updateStmt.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void delete(Question question) {
		try {
			Connection connection = getConnection();
			PreparedStatement delStmt;
			if (question instanceof MCQ)
				delStmt = connection.prepareStatement(DELETE_QUERY_R.replace("tableName", "MCQ"));
			else
				delStmt = connection.prepareStatement(DELETE_QUERY_R.replace("tableName", "Question"));
			delStmt.setInt(1, question.getTopicID());
			delStmt.execute();
			delStmt.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}