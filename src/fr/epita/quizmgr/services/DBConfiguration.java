package fr.epita.quizmgr.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class DBConfiguration {

	private Properties properties;

	private static DBConfiguration instance;

	private DBConfiguration() {
//		this.properties = new Properties();
//		String confLocation = System.getProperty("conf.location");
		File file = new File("app.properties");
		properties = new Properties();
		try {
		properties.load(new FileInputStream(file));
//		try (InputStream is = new FileInputStream(new File(confLocation))) {
//			properties.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static DBConfiguration getInstance() {
		if (instance == null) {
			instance = new DBConfiguration();
		}
		return instance;

	}

	public String getConfigValue(String configKey) {
		return properties.getProperty(configKey);
	}
}
