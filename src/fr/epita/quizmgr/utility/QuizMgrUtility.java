package fr.epita.quizmgr.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import fr.epita.quizmgr.datamodel.Answer;
import fr.epita.quizmgr.datamodel.MCA;
import fr.epita.quizmgr.datamodel.MCQ;
import fr.epita.quizmgr.datamodel.Question;
import fr.epita.quizmgr.services.dao.QuizMgrDAO;

public class QuizMgrUtility {
	private static QuizMgrDAO daoObj = new QuizMgrDAO();

	public void createQuestionAnswers(Scanner scanner) {

		int tableChoice = Integer.parseInt(getResp(scanner, "type of question: \n1.Open Question \n2.MCQ"));
		String questionLabel = getResp(scanner, "Question : ");
		int difficulty = Integer.parseInt(getResp(scanner, "Difficulty : \n1.Simple \n2.Medium \n3.High "));
		int topicID = Integer.parseInt(getResp(scanner, " TopicID: \n1.Java \n2.UML"));
		int updateCount = 0;
		if (tableChoice == 1) {
			createOpenQuestion(scanner, daoObj, questionLabel, difficulty, topicID);
		} else if (tableChoice == 2) {
			createMCQ(scanner, daoObj, questionLabel, difficulty, topicID);
		}
	}

	private void createMCQ(Scanner scanner, QuizMgrDAO daoObj, String questionLabel, int difficulty, int topicID) {
		int updateCount;
		MCQ mcq = new MCQ();
		mcq.setDifficulty(difficulty);
		mcq.setQuestion(questionLabel);
		mcq.setTopicID(topicID);
		List<MCA> multipleAnswersList = new ArrayList<>();
		boolean exitChoice = false;
		do {
			MCA mca = new MCA();
			mca.setAnswer(getResp(scanner, "Answer choice : "));
			mca.setCorrectOption("Y".equalsIgnoreCase(getResp(scanner, "Is this a correct answer:Y/N")) ? true : false);
			multipleAnswersList.add(mca);
			exitChoice = "Q".equalsIgnoreCase(getResp(scanner, "Press q to exit or any key to continue !")) ? true
					: false;
		} while (!exitChoice);
		mcq.setAnswerList(multipleAnswersList);
		updateCount = daoObj.create(null, mcq);

		if (updateCount != 0) {
			List<Question> searchByQuestion = daoObj.searchByQuestion(mcq);
			if (!searchByQuestion.isEmpty()) {
				for (MCA mcaRow : multipleAnswersList) {
					mcaRow.setQuestionID(searchByQuestion.get(0).getQuestionID());
				}
				daoObj.createAnswerForMCQ(multipleAnswersList);
			}

		}
	}

	private void createOpenQuestion(Scanner scanner, QuizMgrDAO daoObj, String questionLabel, int difficulty,
			int topicID) {
		int updateCount = 0;
		Question question = new Question();
		question.setDifficulty(difficulty);
		question.setQuestion(questionLabel);
		question.setTopicID(topicID);
		Answer answer = new Answer();
		answer.setAnswer(getResp(scanner, "Answer : "));
		question.setAnswer(answer);
		updateCount = daoObj.create(question, null);

		if (updateCount != 0) {
			List<Question> searchByQuestion = daoObj.searchByQuestion(question);
			if (!searchByQuestion.isEmpty()) {
				answer.setQuestionID(searchByQuestion.get(0).getQuestionID());
				daoObj.createAnswerForOpenQuestion(answer);
			}

		}
	}

	public void searchQuestionAns(Scanner scanner) {
		boolean loop = true;
		do {
			String searchBy = getResp(scanner, " Search method by:\n1:Topic \n2.ID \n3.keyword \n4:Exit");
			switch (searchBy) {
			case "1":
				searchQuestionsByTopic(scanner, daoObj);
				break;
			case "2":
				searchByID(scanner, daoObj);
				break;
			case "3":
				searchByKeyword(scanner, daoObj);
				break;
			case "4":
				loop = false;
				break;
			default:
				System.out.println("Wrong Selection. Please try again!");
				break;
			}
		} while (loop);
	}

	private void searchByKeyword(Scanner scanner, QuizMgrDAO daoObj2) {
		// TODO Auto-generated method stub
		int tableChoice = Integer.parseInt(getResp(scanner, "type of question: \n1.Open Question \n2.MCQ"));
		Question question;
		if (tableChoice == 1) {
			question = new Question();
			String searchKey = getResp(scanner, " full question");
			question.setQuestion(searchKey);
			List<Question> searchByQuestion = daoObj.searchByQuestion(question);
			if (!searchByQuestion.isEmpty()) {
				searchByQuestion.forEach((temp) -> {
					System.out.println(temp);
				});
			}else {
				System.out.println("0 record found!");
			}
		} else {
			question = new MCQ();
			String searchKey = getResp(scanner, " full question");
			question.setQuestion(searchKey);
			List<Question> searchByQuestion = daoObj.searchByQuestion(question);
			if (!searchByQuestion.isEmpty()) {
				searchByQuestion.forEach((temp) -> {
					System.out.println(temp);
				});
			}else {
				System.out.println("0 record found!");
			}
		}

	}

	private void searchByID(Scanner scanner, QuizMgrDAO daoObj2) {
		int questionID = Integer.parseInt(getResp(scanner, " question ID:"));
		Question question;

	}

	private void searchQuestionsByTopic(Scanner scanner, QuizMgrDAO daoObj) {
		int tableChoice = Integer.parseInt(getResp(scanner, "Topic:\\n1:Java \\n2:UML"));
		Question question;
		if (tableChoice == 1) {
			
		}else {
			
		}

	}

	private static void studentLogin(Scanner scanner) {
		// to do
	}

	private String getResp(Scanner scanner, String proposedQuestion) {
		System.out.println("Kindly enter the " + proposedQuestion);
		return scanner.nextLine();
	}
}
